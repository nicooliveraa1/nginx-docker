
    if ('serviceWorker' in navigator) {
      navigator.serviceWorker.register('/firebase-messaging-sw.js').then(function(registration) {
        // Registration was successful
        console.log('ServiceWorker registration successful with scope: ', registration.scope);
      }).catch(function(err) {
        // registration failed :(
        console.log('ServiceWorker registration failed: ', err);
      });
    }

    MsgElem = document.getElementById("msg");
    TokenElem = document.getElementById("token");
    NotisElem = document.getElementById("notis");
    ErrElem = document.getElementById("err");

    // Initialize Firebase
    // TODO: Replace with your project's customized code snippet
    var config = {
      messagingSenderId: "500670865341",
      apiKey: "AIzaSyCsi3TgB-1xqzU7fYA61kJPm8JWubg2_GQ",
      projectId: "practico2-ej6",
      appId: "1:500670865341:web:886b18bcec1d73a39bfcd4",
    };
    firebase.initializeApp(config);

    const messaging = firebase.messaging();

    function dispatchMensajeRecibido(mensaje) {
      var event = new CustomEvent('mensajeRecibido', {
          detail: {
              mensaje: mensaje
          }
      })
      window.dispatchEvent(event);
  }

    //PRIMER PLANO
    messaging.onMessage((payload) => {
      console.log('Message received. ', payload);
      localStorage.setItem("mensajeDelServidor", JSON.stringify(payload.data) );
      dispatchMensajeRecibido()
    });

    messaging
        .requestPermission()
        .then(function () {
            MsgElem.innerHTML = "Notification permission granted." 
            console.log("PERMISOS DE NOTIFICACION : TRUE");

            // get the token in the form of promise
            return messaging.getToken()
        })
        .then(function(token) {
            TokenElem.innerHTML = "TOKEN FIREBASE : " + token
            //GUARDO TOKEN
            localStorage.setItem("token", token );
        })
        .catch(function (err) {
            ErrElem.innerHTML =  ErrElem.innerHTML + "; " + err
            console.log("PERMISOS DE NOTIFICACION : FALSE", err);
        });