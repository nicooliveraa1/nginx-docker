FROM registry.access.redhat.com/ubi8/nginx-118

ENV NGINX_VERSION=1.18

# Add application sources
ADD 1.18/test-app/nginx.conf "${NGINX_CONF_PATH}"
ADD 1.18/test-app/nginx-default-cfg/*.conf "${NGINX_DEFAULT_CONF_PATH}"
ADD 1.18/test-app/nginx-cfg/*.conf "${NGINX_CONFIGURATION_PATH}"

#ADD 1.18/test-app/*.html /opt/app-root/src
#ADD 1.18/test-app/*.txt /opt/app-root/src
#ADD 1.18/test-app/*.ico /opt/app-root/src
#ADD 1.18/test-app/*.js /opt/app-root/src
#ADD 1.18/test-app/*.json /opt/app-root/src
#ADD 1.18/test-app/*.css /opt/app-root/src
#ADD 1.18/test-app/assets /opt/app-root/src


ADD --chown=777 1.18/test-app/*.html ./ 
ADD --chown=777 1.18/test-app/*.txt ./
ADD --chown=777 1.18/test-app/*.ico ./
ADD --chown=777 1.18/test-app/*.js ./
ADD --chown=777 1.18/test-app/*.json ./
ADD --chown=777 1.18/test-app/*.css ./
RUN mkdir -p ./assets
RUN mkdir -p ./assets/imagenes
ADD --chown=777 1.18/test-app/assets/*.js ./assets
ADD --chown=777 1.18/test-app/assets/imagenes/*.jpg ./assets/imagenes
ADD --chown=777 1.18/test-app/assets/imagenes/*.png ./assets/imagenes

# Run script uses standard ways to run the application
CMD nginx -g "daemon off;"
